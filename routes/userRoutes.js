const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')


//route for checking if the user's email already exists in the database

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
})

//route for user registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


//Route for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

//1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
//Answer:
router.post('/details', (req, res) => {
	userController.getProfile(req.body).then(result => res.send(result))
})



module.exports = router;