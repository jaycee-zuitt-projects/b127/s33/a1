const express = require('express');
//allows our backend application to be available to our frontend application.
//allows us to control app's Cross origin Resource sharing settings
const cors = require('cors')

//allow access to routes defined within our application
const userRoutes = require('./routes/userRoutes')



const mongoose = require('mongoose');
const app = express();


//connect t o our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z7wiz.mongodb.net/Batch127_Booking?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);

//mongoose connection = prompts a messsage in the terminal once the connection is 'open' and we are able to successfully connect to our database.

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))


//allows all resources/origins to access our backend application.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//"/users" to be included for all user routes defined in the "userRoutes" file
//"http://localhost:4000/users"
app.use('/users', userRoutes);




//will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if non is defined.
//This syntax will allow flexibility when using the application locally or as a hosted application.

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})